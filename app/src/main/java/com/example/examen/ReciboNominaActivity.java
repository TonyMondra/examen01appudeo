package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {

    private RadioGroup puestoGrupo;
    private TextView lblEmpleado, lblSubtotal, lblImpuesto, lblTotal;
    private EditText nReciboBox, nombreEmpleadoBox, hNomarlesBox, hExtrasBox;
    private Button botonCalcular, botonLimpiar, botonRegresar;
    private ReciboNomina recibo;
    private RadioButton aux, albanil, inge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        lblEmpleado = findViewById(R.id.lblEmpleado);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblTotal = findViewById(R.id.lblTotal);
        nReciboBox = findViewById(R.id.txtNumRecibo);
        nombreEmpleadoBox = findViewById(R.id.txtNombre);
        hNomarlesBox = findViewById(R.id.txtHorasNormales);
        hExtrasBox = findViewById(R.id.txtHorasExtras);
        botonCalcular = findViewById(R.id.btnCalcular);
        botonLimpiar = findViewById(R.id.btnLimpiar);
        botonRegresar = findViewById(R.id.btnRegresar);
        aux = findViewById(R.id.rdAuxiliar);
        albanil = findViewById(R.id.rdAlbanil);
        inge = findViewById(R.id.rdIng);
        recibo = new ReciboNomina();
        Bundle datos = getIntent().getExtras();
        String empleado = datos.getString("Empleado");
        lblEmpleado.setText("Empleado : " + empleado);


        botonRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        botonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int emptyFields = 0;
                EditText[] editTexts = {nombreEmpleadoBox, nReciboBox, hNomarlesBox, hExtrasBox};


                for (EditText editText : editTexts) {
                    if (TextUtils.isEmpty(editText.getText().toString())) {
                        editText.setError("No puede haber campos vacios");
                        emptyFields++;
                    }

                }

                if (emptyFields == 0 && (aux.isChecked() || albanil.isChecked() || inge.isChecked()))
                {
                    int puesto = 0;

                    if (aux.isChecked()) {
                        puesto = 1;
                    } else if (albanil.isChecked()) {
                        puesto = 2;
                    }
                    else if (inge.isChecked()){
                        puesto = 3;
                    }

                    recibo.setHorasTrabNormal(Float.parseFloat((hNomarlesBox.getText().toString())));
                    recibo.setHorasTrabExtra(Float.parseFloat((hExtrasBox.getText().toString())));
                    recibo.setPuesto(puesto);
                    lblSubtotal.setText("Subtotal: " + String.valueOf(recibo.calcularSubtotal()));
                    lblImpuesto.setText("Impuesto: " + String.valueOf(recibo.calcularImpuesto()));
                    lblTotal.setText("Total: " + String.valueOf(recibo.calcularTotal()));

                    nombreEmpleadoBox.setText("");
                    nReciboBox.setText("");
                    hNomarlesBox.setText("");
                    hExtrasBox.setText("");

                }

                else {
                    Toast.makeText(ReciboNominaActivity.this, "Seleccione un puesto", Toast.LENGTH_SHORT).show();
                }



            }
        });

        botonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nombreEmpleadoBox.setText("");
                nReciboBox.setText("");
                hNomarlesBox.setText("");
                hExtrasBox.setText("");
                lblSubtotal.setText("Subtotal: 0.0");
                lblImpuesto.setText("Impuesto: 0.0");
                lblTotal.setText("Total: 0.0");

            }
        });

    }
}