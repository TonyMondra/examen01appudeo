package com.example.examen;

public class ReciboNomina {

    private  int numRecibo, puesto;
    private String nombre;
    private float horasTrabNormal, horasTrabExtra, impuestoPorc = 16, pagoHoraNormal, pagoHoraExtra;


    public ReciboNomina(){

    }

    public float calcularSubtotal(){

        return  ((this.pagoHoraNormal * this.horasTrabNormal) + (this.pagoHoraExtra * this.horasTrabExtra));
    }

    public float calcularImpuesto(){

        return (this.calcularSubtotal() /100) * this.impuestoPorc ;
    }

    public float calcularTotal(){

        return this.calcularSubtotal() - this.calcularImpuesto();
    }

    public void setHorasTrabNormal(float hNom){
        this.horasTrabNormal = hNom;
    }

    public void setHorasTrabExtra (float hExtra){
        this.horasTrabExtra = hExtra;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;

        if (puesto == 1){
            this.pagoHoraNormal = 50;
            this.pagoHoraExtra = 100;
        }
        else if (puesto == 2){
            this.pagoHoraNormal = 70;
            this.pagoHoraExtra = 140;
        }
        else if (puesto == 3){
            this.pagoHoraNormal = 100;
            this.pagoHoraExtra = 200;
        }
    }

}
