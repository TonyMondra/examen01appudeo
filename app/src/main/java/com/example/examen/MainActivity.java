package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtEmpleado;
    private Button  botonEntrar, botonSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtEmpleado = findViewById(R.id.txtTrabajador);
        botonEntrar = findViewById(R.id.btnEntrar);
        botonSalir = findViewById(R.id.btnSalir);

        botonEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(txtEmpleado.getText().toString().trim().equals(getText(R.string.empleado))){
                    Intent iNomina = new Intent(MainActivity.this, ReciboNominaActivity.class);
                    iNomina.putExtra("Empleado", txtEmpleado.getText().toString().trim());
                    startActivity(iNomina);
                }

                else{
                    Toast.makeText(MainActivity.this, "Revise sus datos: Empleado no existe", Toast.LENGTH_SHORT).show();
                }


            }
        });

        botonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}